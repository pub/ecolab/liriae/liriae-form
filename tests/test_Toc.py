import pytest

from liriae_form.schemas import Toc, TocItem, doc_level


class TestToc:
    @pytest.mark.parametrize(
        "toc, item, expected",
        [
            (
                [TocItem(title="a", page=1, level=1), TocItem(title="b", page=3, level=1)],
                TocItem(title="aa", page=2, level=1),
                [
                    TocItem(title="a", page=1, level=1),
                    TocItem(title="aa", page=2, level=1),
                    TocItem(title="b", page=3, level=1),
                ],
            ),
            (
                [TocItem(title="a", page=1, level=1), TocItem(title="b", page=3, level=1)],
                TocItem(title="aa", page=1, level=1, column=1),
                [
                    TocItem(title="a", page=1, level=1),
                    TocItem(title="aa", page=1, level=1, column=1),
                    TocItem(title="b", page=3, level=1),
                ],
            ),
            (
                [TocItem(title="a", page=1, level=1), TocItem(title="b", page=3, level=1)],
                TocItem(title="c", page=4, level=1),
                [
                    TocItem(title="a", page=1, level=1),
                    TocItem(title="b", page=3, level=1),
                    TocItem(title="c", page=4, level=1),
                ],
            ),
            (
                [TocItem(title="a", page=1, level=1), TocItem(title="b", page=3, level=1)],
                TocItem(title="c", page=4, level=1, column=1),
                [
                    TocItem(title="a", page=1, level=1),
                    TocItem(title="b", page=3, level=1),
                    TocItem(title="c", page=4, level=1, column=1),
                ],
            ),
        ],
    )
    def test_insert_sorted(self, toc, item, expected):
        toc = Toc(items=toc)
        toc.insert_sorted(item)
        assert toc == Toc(items=expected, modified=True)

    @pytest.mark.parametrize(
        "toc, expected",
        [
            ([TocItem(title="1", page=1, level=doc_level)], []),
            (
                [
                    TocItem(title="1", page=1, level=doc_level),
                    TocItem(title="1.1", page=1, level=doc_level + 1),
                    TocItem(title="2", page=1, level=doc_level),
                    TocItem(title="2.1", page=1, level=doc_level + 1),
                ],
                [
                    TocItem(title="1", page=1, level=doc_level),
                    TocItem(title="1.1", page=1, level=doc_level + 1),
                    TocItem(title="2.1", page=1, level=doc_level + 1),
                ],
            ),
        ],
    )
    def test_pop_doc(self, toc, expected):
        toc = Toc(items=toc)
        toc.pop_doc()
        assert toc == Toc(items=expected, modified=True)

    @pytest.mark.parametrize(
        "toc, to_add, expected",
        [
            (
                [
                    TocItem(title="1", page=1, level=0),
                    TocItem(title="1.1.1", page=1, level=2),
                    TocItem(title="1.1.2", page=1, level=2),
                    TocItem(title="2", page=2, level=0),
                    TocItem(title="2.1", page=2, level=1),
                    TocItem(title="2.2", page=2, level=1),
                    TocItem(title="3", page=3, level=0),
                    TocItem(title="3.1.1.1", page=3, level=3),
                ],
                [],
                [
                    TocItem(title="1", page=1, level=0),
                    TocItem(title="unknown", page=1, level=1, origin="fix"),
                    TocItem(title="1.1.1", page=1, level=2),
                    TocItem(title="1.1.2", page=1, level=2),
                    TocItem(title="2", page=2, level=0),
                    TocItem(title="2.1", page=2, level=1),
                    TocItem(title="2.2", page=2, level=1),
                    TocItem(title="3", page=3, level=0),
                    TocItem(title="unknown", page=3, level=1, origin="fix"),
                    TocItem(title="unknown", page=3, level=2, origin="fix"),
                    TocItem(title="3.1.1.1", page=3, level=3),
                ],
            ),
            (
                [
                    TocItem(title="1", page=1, level=0),
                    TocItem(title="unknown", page=1, level=1, origin="fix"),
                    TocItem(title="1.1.1", page=1, level=2),
                ],
                [TocItem(title="1.1", page=1, level=1)],
                [
                    TocItem(title="1", page=1, level=0),
                    TocItem(title="1.1", page=1, level=1),
                    TocItem(title="1.1.1", page=1, level=2),
                ],
            ),
        ],
    )
    def test_add_missing_items(self, toc, to_add: list[TocItem], expected: Toc):
        """Test Toc after some toc items are added and missing items are added/removed"""
        toc = Toc(items=toc)
        for item in to_add:
            toc.insert_sorted(item)
        if not to_add:
            toc.update()
        assert toc == Toc(items=expected, modified=True)

    @pytest.mark.parametrize(
        "toc, doc_index, expected, expected_titles",
        [
            (
                [
                    TocItem(title="1", page=1, level=doc_level),
                    TocItem(title="A", page=1, level=doc_level + 1),
                    TocItem(title="B", page=1, level=doc_level + 2),
                    TocItem(title="A", page=1, level=doc_level + 2),
                    TocItem(title="2", page=2, level=0),
                ],
                0,
                (
                    TocItem(title="1", page=1, level=doc_level),
                    TocItem(title="A", page=1, level=doc_level + 1),
                    TocItem(title="B", page=1, level=doc_level + 2),
                    TocItem(title="A", page=1, level=doc_level + 2),
                ),
                ["1", "A", "B", "A (1)"],
            ),
        ],
    )
    def test_get_doc_toc(self, toc: Toc, doc_index: int, expected, expected_titles):
        toc = Toc(items=toc)
        doc = toc[doc_index]
        assert toc.get_doc_toc(doc) == expected
        titles = toc.get_titles(doc=doc)
        assert titles == expected_titles

    @pytest.mark.parametrize(
        "toc",
        [
            [
                TocItem(title="1", page=1, level=0),
                TocItem(title="1.1", page=1, level=1),
                TocItem(title="1.1.1", page=1, level=2),
            ],
        ],
    )
    def test_load(self, toc):
        toc = Toc(items=toc)
        # TODO: test TocItem.toc
        assert toc == Toc.parse_raw(toc.json())

    def test_toc_reference(self):
        toc = Toc(items=[TocItem(title="1", page=1, level=0)])
        for item in toc.items:
            assert item.parent == toc
        toc.insert_sorted(TocItem(title="1.1.1", page=1, level=2))
        for item in toc.items:
            assert item.parent == toc
