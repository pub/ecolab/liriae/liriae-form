from liriae_form.schemas import PdfFile


def test_reference():
    """Check if toc has a reference to PdfFile"""
    pdf_file = PdfFile.parse_obj({"name": "file.pdf", "path": "/tmp/file.pdf", "n_pages": 1, "size_MB": 0.0, "toc": []})
    assert pdf_file.toc.parent == pdf_file


def test_flag_validate():
    pdf_file = PdfFile.parse_obj({"name": "file.pdf", "path": "/tmp/file.pdf", "n_pages": 1, "size_MB": 0.0, "toc": []})
    pdf_file.validated = True
    assert pdf_file.validated
    pdf_file.flagged = True
    assert not pdf_file.validated
    pdf_file.set_validated()
    assert pdf_file.validated
    assert not pdf_file.flagged
