from contextlib import nullcontext as does_not_raise

import pytest
from pydantic import ValidationError

from liriae_form.schemas import TocItem


@pytest.mark.parametrize(
    "values, expectation",
    [
        (dict(title="1", page=1, level=0), does_not_raise()),
        (dict(title="", page=1, level=0), pytest.raises(ValidationError)),
        (dict(title="1", page=0, level=0), pytest.raises(ValidationError)),
        (dict(title="1", page=1, level=-1), pytest.raises(ValidationError)),
    ],
)
def test_validations(values, expectation):
    with expectation:
        assert TocItem.parse_obj(values) is not None


def test_pos():
    items = [TocItem(title="a", page=1, level=1), TocItem(title="b", page=3, level=1)]
    assert sorted(items) == items
    assert sorted([items[1], items[0]]) == items
