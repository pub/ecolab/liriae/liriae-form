import os

import streamlit as st
from streamlit_option_menu import option_menu

from liriae_form.schemas import Submission, TocItem, doc_level
from liriae_form.st_utils import change_toc_item_status, display_pdf, warning_same_doc_names
from liriae_form.utils import output_dir, run_pdfstruct, set_values, version


def page3():

    # Load state
    submission: Submission = st.session_state.submission

    st.set_page_config(f"LIRIAe form (v{version})", layout="wide")
    st.title("🌳 LIRIAe: liseuse et recherche intelligente pour les autorités environnementales")
    st.subheader(f"Formulaire de dépôt et modification de dossiers (v{version})")

    st.header("Découpage et modification de vos documents")
    st.info(f"Vous traitez le dossier: {submission.request_id}")

    with st.sidebar:
        pdf_selected = option_menu("Vos fichiers pdf", submission.files, menu_icon="cast", default_index=0)

        if any(not (pdf_file.validated or pdf_file.flagged) for pdf_file in submission.pdf_files.values()):
            st.info("Vous devez valider ou signaler tous les pdf du menu pour pouvoir passer à l'étape suivante")

    # Initialization with pdfstruct if necessary and once
    if pdf_selected not in submission.info_pdfstruct or st.button(
        "Refaire la detection des titres",
        help="Attention: la detection va être relancée pour ce pdf et vous allez perdre vos modifications",
    ):
        with st.spinner("Traitement de votre fichier en cours ..."):
            run_pdfstruct(
                submission=submission,
                pdf_path=os.path.join(output_dir, submission.request_id, pdf_selected),
                pdf_selected=pdf_selected,
            )

    # Default tools to modify the pdf
    pdf_file = submission.pdf_files[pdf_selected]
    toc = pdf_file.toc

    # Modification Menu for documents
    st.header("Reconnaissance de vos documents")
    st.markdown(
        "Un pdf peut contenir un ou plusieurs documents (résumé non technique, ensemble d'annexes, ...). \
        Ci-dessous vous trouverez une proposition de collection de documents que vous pouvez modifier si besoin."
    )

    # Columns to lay out the inputs
    col1, col2, col3 = st.columns([4, 1.6, 1.6])
    col1.write("**Nom document**")
    col2.write("**1ère page**")
    col3.write("**Doc utile**")

    for doc_number, doc in enumerate(toc.docs):
        # No overlap between docs
        min_value = 1 if doc_number == 0 else toc.docs[doc_number - 1].page + 1
        max_value = (
            1
            if doc_number == 0  # first doc
            else pdf_file.n_pages
            if doc_number == toc.n_docs - 1  # last doc
            else toc.docs[doc_number + 1].page - 1
        )

        col1.text_input(
            "",
            value=doc.title,
            key=f"doc_title{doc_number}",
            on_change=set_values,
            kwargs=dict(doc=doc, doc_number=doc_number, state=st.session_state),
        )
        col2.number_input(
            "",
            min_value=min_value,
            max_value=max_value,
            value=doc.page,
            key=f"doc_page{doc_number}",
            on_change=set_values,
            kwargs=dict(doc=doc, doc_number=doc_number, state=st.session_state),
        )

        col3.selectbox(
            "",
            ["✅", "❌"] if doc.keep else ["❌", "✅"],
            key=f"keep_doc{doc_number}",
            on_change=set_values,
            kwargs=dict(doc=doc, doc_number=doc_number, state=st.session_state),
        )

    # Check if all titles have different names
    warning_same_doc_names(pdf_file)  # TODO: disable validation

    col1, col2, col3, col4 = st.columns(4)
    col1.button(
        "Retirer un document",
        on_click=toc.pop_doc,
        disabled=pdf_file.n_pages == 1 or toc.n_docs == 1,
    )
    col2.button(
        "Ajouter un document",
        on_click=lambda: toc.add_doc(n_pages=pdf_file.n_pages),
        disabled=(pdf_file.n_pages == toc.docs[toc.n_docs - 1].page),
    )
    col3.button(
        "✅ Valider le fichier pdf",
        on_click=pdf_file.set_validated,
        disabled=pdf_file.validated or pdf_file.flagged,
        help="Valider le fichier signifie que tous les documents et titres sont ok"
        " (après les modifications manuelles, si besoin)",
    )
    col4.button(
        "🚩 Signaler le fichier pdf 🚩",
        help="""En signalant le fichier, vous indiquez que le traitement automatique
        a très mal mené et que trop de modifications manuelles sont à apporter
        pour qu'il soit recevable """,
        on_click=lambda: pdf_file.set_flagged(),
        disabled=pdf_file.flagged,
    )
    if pdf_file.flagged:
        st.subheader("Signalement du fichier pdf")
        st.warning(
            "Ce fichier sera traité manuellement par nos équipes."
            " Vous pouvez tout de même poursuivre le traitement et avoir accès"
            " à votre dossier."
            " Nous reviendrons vers vous dans les plus brefs délais",
        )
        st.text_area(
            "Expliquez nous pourquoi le document n'est pas acceptable",
            value=pdf_file.flagged_description,
            key="description",
            on_change=lambda: pdf_file.set_flagged_description(st.session_state.get("description", "")),
        )
        st.button(
            "🔁 Annulation du signalement",
            on_click=lambda: pdf_file.set_flagged(False),
        )

    # Modification of docs selected
    docs_kept = [i.title for i in toc.docs_kept]
    if not docs_kept:
        return

    st.header("Modification des documents sélectionnés")
    col1, col2 = st.columns(2)
    with col1:
        doc_selected = option_menu(
            menu_title="Vos documents",
            options=docs_kept,
        )
        index_doc = docs_kept.index(doc_selected)
        doc = toc.docs_kept[index_doc]
        max_page = toc.docs_kept[index_doc + 1].page if len(toc.docs_kept) >= index_doc + 2 else pdf_file.n_pages
        doc_toc = toc.get_doc_toc(doc)
        titles = toc.get_titles(add_icon=True, level_sep="--", doc=doc)

    if len(titles) == 1:
        col2.warning(
            """Aucun titre n'a été détecté sur ce document, vous pouvez en ajouter manuellement ou
            passer au traitement d'un autre document."""
        )

    levels_available = list(set(item.level for item in doc_toc))

    col1.subheader("Affichage des titres")
    max_level = col1.number_input(
        "Niveau max titres",
        min(levels_available),
        max(levels_available),
        value=doc_level + (1 if levels_available else 0),
        help='Le "Niveau max du titre" correspond au niveau maximal du titre que \
            vous souhaitez afficher, allant de macro (I) à micro (I.A.1.a)',
    )

    options = [title for item, title in zip(doc_toc, titles) if item.level <= max_level]
    title_selected = col1.selectbox("Titre sélectionné", options=options)
    title_index = titles.index(title_selected or "")
    toc_item = doc_toc[title_index]

    # Page change
    page = col2.number_input(
        "Page",
        min_value=doc.page,
        max_value=max_page,
        value=toc_item.page,
        key="page_displayed",
    )

    display_pdf(
        path=os.path.join(output_dir, submission.request_id, pdf_selected),
        page=int(page),  # for mypy
        columns=[col1, col2],
    )

    if toc_item.level > doc_level and toc_item.origin:
        container = col1.container()
        container.subheader("Modification du titre")
        if toc_item.origin == "fix":
            container.warning(
                "Ce titre a été ajouté automatiquement pour corriger un problème avec les niveaux. "
                "Vous avez la possibilité de rajouter un titre à la bonne position et "
                "celui-ci sera automatiquement supprimé"
            )
        choices = (
            "Le titre est impeccable ✅",
            "Modifier le titre ⏭",
            "Ce n'est pas un titre ❌",
        )

        container.radio(
            "",
            options=choices,
            index=2 if not toc_item.keep else 1 if toc_item.modified else 0,
            on_change=change_toc_item_status,
            kwargs=dict(
                toc_item=toc_item,
                max_level=max(levels_available),
                container=container,
            ),
            key="title_change",
        )
        # FIXME: show modification options here, add button with on_click=change_toc_item

    # Add title
    expander = col1.expander("Ajouter un titre dans le document")
    title_to_add = expander.text_input("Titre", value="")
    level_to_add = expander.number_input(
        "Niveau",
        min_value=doc_level + 1,
        max_value=max(levels_available) + 1,
    )

    expander.button(
        "Valider l'ajout",
        on_click=lambda: toc.insert_sorted(
            TocItem(title=title_to_add, level=level_to_add, page=st.session_state.get("page_displayed"))
        ),  # TODO: add position
        disabled=not title_to_add,
    )
