from __future__ import annotations

from itertools import pairwise

from _bisect import bisect_left

from .base import Position, Updater, doc_level
from .TocItem import TocItem


class Toc(Updater):
    """
    Table of contents, list-like plus:
     - reference pdf_file to PdfFile: None by default. Extra field as __root__ does not accept other fields
     - modified (bool, default=False): Extra field as __root__ does not accept other fields
     - toc items are always sorted
     - holes between items, in terms of level, are filled automatically. Example:
       [TocItem(..., level=1), TocItem(..., level=3)] ->
       [TocItem(..., level=1), TocItem(title="unknown", level=2, origin="fix", ...),TocItem(..., level=3)]
    - all items hold a reference to the toc
    """

    items: list[TocItem] = []  # WARNING: do not append to the list directly, use insert_sorted instead

    def __init__(self, **kwargs):
        super(Toc, self).__init__(**kwargs)
        for item in self.items:
            if item.parent is None:
                item.parent = self

    def __iter__(self):
        return self.items.__iter__()

    def __getitem__(self, item: int) -> TocItem:
        return self.items[item]

    def __len__(self):
        return self.items.__len__()

    def index(self, item: TocItem) -> int:
        return self.items.index(item)

    def insert_sorted(self, item: TocItem):
        """Add TocItem in the right position to keep the Toc sorted and without holes in levels"""
        item.parent = self
        self.items.insert(bisect_left(self.items, item), item)
        if item.origin != "fix":
            self.update()

    def _add_missing_items(self):
        """Add missing items (holes in levels) as 'unknown' before the next item
        and remove unnecessary ones"""
        needed = {
            TocItem(
                # title=f"unknown_{next_item.page}_{level}",
                title="unknown",
                page=next_item.page,
                level=level,
                origin="fix",
                position=Position(x=next_item.position.x, y=next_item.position.y),
            )
            for item, next_item in pairwise(_ for _ in self.items if _.keep and not _.origin == "fix")
            if next_item.level - item.level > 1
            for level in range(item.level + 1, next_item.level)
        }
        present = {item for item in self.items if item.origin == "fix"}

        # To add: needed not in present
        for item in set.difference(needed, present):
            self.insert_sorted(item)
        # To remove: present not in needed
        for item in set.difference(present, needed):
            self.items.remove(item)

    def update(self, parent: bool = True):
        self.items.sort()
        self._add_missing_items()
        super().update(parent=parent)

    @property
    def docs(self):
        return [item for item in self.items if item.level == doc_level]

    @property
    def docs_kept(self):
        return [item for item in self.items if item.level == doc_level and item.keep]

    @property
    def n_docs(self):
        return len(self.docs)

    def pop_doc(self):
        """Remove the last document"""
        self.items.remove(self.docs[-1])
        self.update()

    def add_doc(self, n_pages: int, title: str | None = None) -> None:
        """Add a new document. By default, name it "Document_{n_docs + 1} and use the next page from the previous doc"""
        page = self.docs[-1].page + 1 if self.docs else n_pages
        self.insert_sorted(
            TocItem(
                title=title if title is not None else f"Document_{self.n_docs + 1}",
                page=page,
                level=doc_level,
                origin="user",
            )
        )
        self.update()

    def get_titles(self, add_icon: bool = False, level_sep: str = "", doc: TocItem | None = None) -> list[str]:
        """Return  a list of unique titles, appending adding (1), (2), ... if needed.

        Args:
            add_icon (bool, default=False): prepend TocItem icon to each title
            level_sep (str, default="": prepend N-level (- doc_level) times the given separator to each title
            doc (TocItem or None, by default): return only the titles for the given doc

        Returns: list of str with titles
        """
        titles: list[str] = []
        for item in self.items:
            if doc is not None and item != doc and not titles:
                continue
            elif doc is not None and item != doc and item.level == doc_level:
                break
            title = item.get_title(add_icon=add_icon, level_sep=level_sep)
            count = titles.count(title)
            titles.append(title if not count else f"{item.title} ({count})")
        return titles

    def get_doc_toc(self, doc: TocItem) -> tuple[TocItem, ...]:
        """Return a tuple with the toc items belonging to the given document"""
        assert doc.level == doc_level, f"Not a document: {doc}"
        doc_index = self.index(doc)
        toc = []
        for item in self.items[doc_index:]:
            if item != doc and item.level == doc_level:
                break
            toc.append(item)
        return tuple(toc)
