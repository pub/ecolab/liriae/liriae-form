from datetime import datetime
from typing import Literal

from pydantic import BaseModel, EmailStr, Field

from .base import Updater
from .PdfFile import PdfFile


class SubmissionBase(BaseModel):
    """Submission information for gitlab issue"""

    date: datetime
    request_id: str
    email: EmailStr | Literal[""]
    region: str
    name: str
    files: tuple[str, ...]
    etude_dimpact: list[str]
    size_MB: float = 0.0


class InfoPdfstruct(BaseModel):
    pdfstruct_version: str = ""
    pdf_name: str
    time_running: float
    n_docs: int
    docs_first_pages: list[int]
    collection: list = Field([], exclude=True)  # exclude from outputs


class Submission(SubmissionBase, Updater):
    __skip_update__: list[str] = ["modified", "validated", "size_MB"]
    output_dir: str
    new: bool = True
    pdf_files: dict[str, PdfFile] = {}
    info_pdfstruct: dict[str, InfoPdfstruct] = {}
    issue_iid: int = -1
    validated: bool = False

    @property
    def ready_to_merge(self):
        """Return True if ready to merge: all files validated and at least one document kept"""
        # if self.validated:  # TODO: enable when it is set to False everywhere when something changes
        #     return False
        return all(pdf_file.validated or pdf_file.flagged for pdf_file in self.pdf_files.values()) and any(
            item.keep for pdf_file in self.pdf_files.values() for item in pdf_file.toc.docs
        )

    def add_file(self, name: str, path: str, n_pages: int, size_MB: float):
        self.pdf_files[name] = PdfFile(name=name, path=path, n_pages=n_pages, size_MB=size_MB, toc=[], parent=self)
        self.files = self.files + (name,)

    def update(self, parent: bool = True):
        self.modified = True
        self.validated = False
        self.size_MB = sum(i.size_MB for i in self.pdf_files.values())
