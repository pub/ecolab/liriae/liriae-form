from .base import Updater
from .Toc import Toc


class PdfFile(Updater):
    """BaseModel with information about pdf file, including its toc, and a reference to the submission.
    Should be validated to complete the submission and validated is set to False at each change in its value
    or in the toc items.

    WARNING: in order to keep consistency between 'validated' and 'flagged', do not set validated=True explicitly.
    Use set_validated() instead

    """

    __skip_update__: list[str] = ["modified", "validated"]
    name: str
    path: str
    size_MB: float
    n_pages: int
    toc: Toc
    validated: bool = False
    flagged: bool = False
    flagged_description: str = ""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.toc is not None:
            self.toc.parent = self

    def set_validated(self, validated: bool = True):
        self.flagged = False  # will call update
        self.validated = validated  # need to call last, otherwise other assignments will set it to true

    def set_flagged(self, flagged: bool = True):
        self.flagged = flagged

    def set_flagged_description(self, flagged_description: str):
        self.flagged_description = flagged_description

    @property
    def n_docs(self):
        return self.toc.n_docs

    def update(self, parent: bool = True):
        super().update(parent=parent)
        self.validated = False
