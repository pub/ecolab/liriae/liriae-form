from typing import TYPE_CHECKING, Any, Literal

from pydantic import Field, NonNegativeInt, PositiveInt, constr, root_validator

from .base import Position, Updater, doc_level


class TocItem(Updater):
    """Item in table of contents: document (level=doc_level), section, sub-section, ...

    Origin can be:
    - pdfstruct: detected by pdfstruct
    - fix: holes in levels, fixed 'manually'
    - user: added by the user
    """

    __hash__ = object.__hash__  # for sorting and comparisons
    if TYPE_CHECKING:
        title: str  # for mypy
    else:
        title: constr(strip_whitespace=True, min_length=1)
    page: PositiveInt  # 1-based
    level: NonNegativeInt
    column: NonNegativeInt = 0
    position: Position = Position()
    keep: bool = True
    original_title: str | None = Field(None, allow_mutation=False)
    original_level: int | None = Field(None, allow_mutation=False)
    origin: Literal["pdfstruct", "fix", "user"] = "pdfstruct"

    class Config:
        validate_assignment = True

    @root_validator(allow_reuse=True)
    def set_original_values(cls, values: dict[str, Any]) -> dict[str, Any]:
        """Set original_X to X if not set"""
        for key in values.keys():
            if key.startswith("original_") and values[key] is None and key.replace("original_", "") in values:
                values[key] = values[key.replace("original_", "")]
        return values

    def reset(self):
        """Set values back to original ones"""
        self.title = self.original_title or ""
        self.level = self.original_level or -1
        self.keep = True
        self.modified = False  # need to call last, otherwise other assignments will set it to true

    @property
    def pos(self):
        """Pseudo-position to sort toc items"""
        return self.page, self.column, self.position.y, self.level

    def __lt__(self, other):
        return self.pos < other.pos

    @property
    def icon(self):
        """Return an icon to display item status: ✅ (ok), ⏭ (modified) or ❌ (keep=False)"""
        if not self.keep:
            return "❌"
        if self.modified:
            return "⏭"
        return "✅"

    def get_title(self, add_icon: bool = False, level_sep: str = "") -> str:
        """Return the item title, prepended by the icon and a level separator, if requested

        Args:
            add_icon (bool, default=False): prepend title with item icon
            level_sep (str, default="": prepend title with N-level (- doc_level) times
                                        the given separator to each title

        """

        if add_icon:
            return f"{self.icon} {self.get_title(False, level_sep)}"
        if level_sep:
            return f"{level_sep * (self.level - doc_level)} {self.get_title(False, '')}"
        return self.title
