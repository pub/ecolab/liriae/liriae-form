from __future__ import annotations

from typing import Any

from pydantic import BaseModel, Field

doc_level = 0  # level of documents in toc


class Position(BaseModel):
    x: float = 0.0
    y: float = 0.0


class Updater(BaseModel):
    """BaseModel with modified (bool, default=False), parent (Updater, default=None) and update() method.
    update() is called when any attribute of the class is changed, except at the c-tor and for those in __skip_update__.
     It sets modified=True and calls parent.update().

    N.B.: to avoid infinite recursion, all variables that are changed at update() should be listed in __skip_update__.
    And when changing those variables, it needs to be done *after* all other assignments.
    """

    __skip_update__: list[str] = ["modified"]
    modified: bool = False
    parent: Updater | None = Field(default=None, allow_mutation=False, exclude=True)  # exclude from outputs

    class Config:
        validate_assignment = True

    def update(self, parent: bool = True):
        self.modified = True
        if parent and self.parent is not None:
            self.parent.update()

    def __setattr__(self, key: str, value: Any) -> None:
        """Redefine the assignment operator to call self.update()"""
        object.__setattr__(self, key, value)
        if key not in self.__skip_update__ and not hasattr(value, "update"):  # to avoid infinite recursion
            self.update()
        # if key not in self.__skip_update__:
        #     self.update(parent=not hasattr(value, "update"))  # to avoid infinite recursion
