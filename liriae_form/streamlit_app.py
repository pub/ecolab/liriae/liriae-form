import streamlit as st

from liriae_form.accueil import page1
from liriae_form.depot import page2
from liriae_form.merge import page4
from liriae_form.modification import page3

page_names_to_funcs = {"accueil": page1, "depot": page2, "modification": page3, "merge": page4}


def switch_page_previous():
    pages = list(page_names_to_funcs.keys())
    current_index = pages.index(st.session_state.current_page)
    st.session_state.current_page = pages[current_index - 1]


def switch_page_next():
    pages = list(page_names_to_funcs.keys())
    current_index = pages.index(st.session_state.current_page)
    st.session_state.current_page = pages[current_index + 1]


if "current_page" not in st.session_state:
    st.session_state.current_page = "accueil"

pages = list(page_names_to_funcs.keys())
current_index = pages.index(st.session_state.current_page)
page_names_to_funcs[st.session_state.current_page]()


def condition_next(current_index: int, pages: list):
    """returns a bool that can disable the button to go to the next page depending on the page
    Args:
        current_index (int): current index page
        pages (list): list of all pages
    Returns:
        bol: if the button to switch to the next page is disabled
    """

    if current_index == 1 and "submission" not in st.session_state:
        return True
    elif current_index == 2 and (
        "submission" not in st.session_state or not st.session_state.submission.ready_to_merge
    ):
        return True
    elif current_index == (len(pages) - 1):  # stop if it's the last page
        return True
    return False


def condition_previous(current_index):
    if current_index == 0:
        return True
    else:
        return False


choice_next = condition_next(current_index, pages)
choice_previous = condition_previous(current_index)

col1, col2, col3 = st.columns([2, 7, 2])
with col1:
    st.button("Page précédante", on_click=switch_page_previous, disabled=choice_previous)

with col3:
    st.button("Page suivante", on_click=switch_page_next, disabled=choice_next)
