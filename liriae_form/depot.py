import streamlit as st
from streamlit_sortables import sort_items

from liriae_form.schemas import Submission
from liriae_form.st_utils import check_email, create_submission, load_submission
from liriae_form.utils import regions, version


def page2():
    st.set_page_config(f"LIRIAe form (v{version})")
    st.title("🌳 LIRIAe: liseuse et recherche intelligente pour les autorités environnementales")
    st.subheader(f"Formulaire de dépôt et modification de dossiers (v{version})")
    st.header("Construction de votre dossier")

    if "submission" in st.session_state:
        submission: Submission = st.session_state.submission
        st.info(f"Vous traitez le dossier: {submission.request_id}")
        st.subheader("Ordre des fichiers déposés")
        st.markdown(
            """L'ordre de l'enchaînement des différents pdf est absolument essentiel pour votre confort de
             lecture et sa cohérence. Nous vous demandons ici de glisser les cases les unes par rapport aux
             autres pour choisir l'ordre logique."""
        )
        submission.files = tuple(sort_items(list(submission.files)))
    else:
        choices = ("Soumettre un nouveau dossier", "Modifier un ancien dossier")
        if st.radio("", choices, horizontal=True) == "Soumettre un nouveau dossier":
            st.subheader("📄 Dépôt de vos pdfs bruts")
            st.write(
                """Ce formulaire permet de récupérer quelques informations essentielles sur votre dépôt ainsi
                que les pièces qui composeront votre pdf final.
                """
            )

            # Callback function
            def get_files():
                st.session_state.files = [f.name for f in st.session_state["uploaded_files"]]

            st.text_input(
                "Votre email (facultatif): ",
                key="email",
                on_change=lambda: check_email(st.session_state.get("email", "")),
                help="""En saisissant votre mail, nous pourrons revenir vers vous si vous rencontrez un problème durant
                le traitement""",
            )
            st.selectbox("Votre région*", [""] + regions, key="region")
            st.text_input("Nom du dossier ou numéro Garance*", key="name")

            st.file_uploader(
                "Pièces en format pdf: ",
                accept_multiple_files=True,
                type=".pdf",
                key="uploaded_files",
                help="""Vous pouvez déposer des fichiers et dossiers entiers, contenant des sous-dossiers
                et fichiers de tous les types. Les fichiers autres que pdf seront ignorés.""",
                on_change=get_files,
            )

            if st.session_state.get("files", False):
                st.write(
                    "Pièce(s) contenant l'étude d'impact environnemental (pour les outils de traitement de la \
                    langue): "
                )
                st.session_state.etude_dimpact = [i for i in st.session_state.files if st.checkbox(i)]
            # url = "https://git.lab.sspcloud.fr/liriae/liriae-conditions/-/blob/main/README.md"
            # st.checkbox(f"J'accepte les [conditions d'utilisation]({url}) de LIRIAe", key="conditions")

            st.button(
                "Envoyer",
                on_click=create_submission,
                disabled=not st.session_state.region
                or not st.session_state.name
                or not st.session_state.get("files")
                or not check_email(st.session_state.get("email", "")),
            )

        else:
            st.subheader("💾 Connexion à un dossier existant")
            container = st.container()
            container.text_input("", key="request_id", placeholder="Numéro de dossier")
            container.button(
                "Valider",
                on_click=load_submission,
                kwargs=dict(container=container),
            )
