import base64
import os
import uuid
from datetime import datetime

import fitz
import requests
import streamlit as st
from pydantic import EmailError, EmailStr
from smart_open import open
from streamlit.delta_generator import DeltaGenerator

from liriae_form.schemas import PdfFile, Submission, TocItem, doc_level
from liriae_form.utils import (
    change_toc_item,
    concatenate_pdf,
    file_exists,
    get_s3_client,
    load_json,
    open_and_check_issue,
    open_pdf,
    output_dir,
    write_file,
    write_json,
)


def receive_files():
    submission = st.session_state.submission
    progress_bar = st.progress(0.0)
    uploaded = 0.0
    total_size = 1.0 * sum(file.size for file in st.session_state["uploaded_files"])
    for uploaded_file in st.session_state["uploaded_files"]:
        name = uploaded_file.name
        output_file_name = os.path.join(output_dir, submission.request_id, uploaded_file.name)
        write_file(uploaded_file, output_file_name)
        uploaded += uploaded_file.size / total_size
        progress_bar.progress(uploaded)

        with open_pdf(output_file_name) as doc:
            n_pages = doc.page_count
        submission.add_file(name=name, path=output_file_name, n_pages=n_pages, size_MB=uploaded_file.size / 1e6)


def create_submission():
    request_id = str(uuid.uuid4())
    submission = Submission(
        date=datetime.utcnow(),
        request_id=request_id,
        email=st.session_state.email,
        region=st.session_state.region,
        name=st.session_state.name,
        files=[],
        etude_dimpact=st.session_state.etude_dimpact,
        output_dir=os.path.join(output_dir, request_id),
    )
    st.session_state["submission"] = submission
    receive_files()
    write_json(submission)


def load_submission(container):
    request_id = st.session_state.request_id.strip()
    json_path = os.path.join(output_dir, request_id, f"FINAL_{request_id}.json")
    if not request_id:
        container.error("Veuillez saisir un numéro de dossier")
    elif not file_exists(json_path):
        container.error(
            """Votre numéro de dossier est incorrect ou vous n'avez pas correctement
        sauvegardé votre dossier précédant"""
        )
    else:
        container.success("""Vous êtes connecté à votre dossier, vous pouvez aller à la page suivante""")
        st.session_state.submission = load_json(json_path)


def check_email(email: str):
    try:
        return email == "" or EmailStr.validate(email)
    except (TypeError, EmailError):
        st.warning("Le format de l'email n'est pas valable")


def warning_same_doc_names(pdf_file: PdfFile):
    """write an alarm if 2 titles are the same"""
    doc_names = [item.title for item in pdf_file.toc.docs]
    for name in doc_names:
        if doc_names.count(name) > 1:
            st.session_state[f"{pdf_file.name}/validated/titles"] = False
            return st.error("⚠ Tous les noms de documents doivent être différents pour continuer !")
    st.session_state[f"{pdf_file.name}/validated/titles"] = True


def display_pdf(path: str, page: int = 1, columns=None, width: float | None = None, height: float | None = None):
    """display the correct pdf
    Args:
        path (str): pdf path
        page (int, optional) -> page to display (1-based)
        columns (str columns, optional): predefined columns to use. Defaults to None.
        width (float, optional): width of canvas in pixels,
                                 taken from the page aspect ratio if height is provided, otherwise 800
        height (float, optional): height of canvas in pixels, taken from the page aspect ratio by default
    """
    try:
        col1, col2 = columns[:2]
    except TypeError:
        col1, col2 = st.columns([1, 3])
    with col2:
        page -= 1
        with fitz.open() as doc2, open_pdf(path) as doc:
            doc2.insert_pdf(doc, page, page)
            content = doc2.write()
            if width is None or height is None:
                p = doc.load_page(page)
                w, h = p.rect[2:]
                if width is None:
                    width = 1.0 * w / h * height if height is not None else 800.0
                if height is None:
                    height = 1.0 * h / w * width
        base64_pdf = base64.b64encode(content).decode("utf-8")
        # Embedding PDF in HTML
        pdf_display = f"""<embed src="data:application/pdf;base64,
            {base64_pdf}" width="{1.2 * width}" height="{1.2 * height}"
            type="application/pdf"></embed>"""
        # Displaying File
        st.markdown(pdf_display, unsafe_allow_html=True)


def send_final_doc(pdf_id: str, doc: bytes, n_toc_items: int, index=True, index_annotations=True, use_liriae_back=True):
    liriae_pdf_url = os.environ.get("LIRIAE_PDF_URL", "").rstrip("/")
    liriae_back_url = os.environ.get("LIRIAE_BACK_URL", "").rstrip("/")
    if use_liriae_back and not liriae_back_url:
        st.error("LIRIAE_BACK_URL non renseignée")
        return False

    username = os.environ.get("LIRIAE_PDF_USER")
    password = os.environ.get("LIRIAE_PDF_USER_PWD")
    if not username or not password:
        st.error("Nom d'utilisateur et/ou mot-de-passe pour liriae-pdf non renseigné(s)")
        return False

    response = requests.post(f"{liriae_pdf_url}/token", data={"username": username, "password": password})
    if response.status_code != 200:
        st.error(f"Problème d'authentification à {liriae_pdf_url}: {response.json()}")
        return False
    token = response.json()["access_token"]
    headers = {"Authorization": f"Bearer {token}"}

    response = requests.post(
        f"{liriae_pdf_url}/pdf/{pdf_id}?overwrite=True",
        files={"file": (f"{pdf_id}.pdf", doc, "application/pdf")},
        headers=headers,
    )
    if response.status_code != 200:
        st.error(f"Problème avec l'envoi du pdf: {response.json()}")
        return False

    if index:
        progress_bar = st.progress(0.0, text=f"Sections indéxées: 0 / {n_toc_items}")
        if use_liriae_back:
            index_content = False
            index_annotations = False
            with st.spinner("Indexation du contenu et des annotations en cours..."):
                response = requests.post(f"{liriae_back_url}/api/pdf/index?docId={pdf_id}")
                if response.status_code // 100 != 2:
                    st.error(f"Problème avec l'indexation (liriae-back): {response.text}")
        else:
            index_content = True

        for section in range(n_toc_items):
            response = requests.post(
                f"{liriae_pdf_url}/index/{pdf_id}?section_id={section}"
                f"&index_content={index_content}&index_words=True&index_annotations={index_annotations}",
                headers=headers,
            )
            if response.status_code != 200:
                st.error(f"Problème avec l'indexation de la section {section}: {response.text}")
                return False
            progress_bar.progress(1.0 * section / n_toc_items, text=f"Sections indéxées: {section + 1} / {n_toc_items}")
    return True


def change_toc_item_status(toc_item: TocItem, max_level: int, container: DeltaGenerator):
    """
    Change toc item status (keep, flag, modify)

    Args:
        toc_item: toc item to change
        max_level: maximum level allowed (maximum in toc + 1)
        container: streamlit container to add the widgets
    """
    # TODO: choices on top of utils
    choice = st.session_state["title_change"]
    if choice == "Le titre est impeccable ✅":
        toc_item.reset()
    elif choice == "Ce n'est pas un titre ❌":
        toc_item.keep = False
    elif choice == "Modifier le titre ⏭":
        if not toc_item.keep:
            toc_item.keep = True
        container.number_input(
            "Niveau",
            key="toc_item_level",
            value=toc_item.level,
            min_value=doc_level + 1,
            max_value=max_level + 1,
            on_change=change_toc_item,
            args=(toc_item, st.session_state),
        )
        container.text_input(
            "Titre",
            key="toc_item_title",
            value=toc_item.title,
            on_change=change_toc_item,
            args=(toc_item, st.session_state),
        )
        # container.button(
        #     "Valider la modification",
        #     on_click=change_toc_item,
        #     kwargs=dict(title=title, level=level),
        #     disabled=level == toc_item.level and (not title or title == toc_item.title),
        # )
    else:
        raise ValueError(f"Invalid choice: {choice}")


def validate_submission(submission: Submission):
    """
    Finalise and validate submission:
    - concatenate and write final pdf
    - open issue on gitlab
    - write json file
    """
    request_id = submission.request_id
    final_doc = concatenate_pdf([submission.pdf_files[i] for i in submission.files])
    final_path_pdf = os.path.join(submission.output_dir, "FINAL_" + request_id + ".pdf")
    with open(final_path_pdf, mode="wb", transport_params={"client": get_s3_client()}) as f:
        f.write(final_doc.write())

        # Send file to liriae-pdf and index
        if st.session_state.get("do_send_to_api"):
            if (
                send_final_doc(
                    pdf_id=request_id,
                    doc=final_doc.write(),
                    n_toc_items=len(final_doc.get_toc()),
                    index=st.session_state.get("do_index", True),
                    index_annotations=st.session_state.get("do_index_annotations", True),
                    use_liriae_back=st.session_state.get("use_liriae_back", True),
                )
                and "LIRIAE_URL" in os.environ
            ):
                url = f'{os.environ["LIRIAE_URL"].rstrip("/")}/{request_id}'
                if requests.get(url).status_code == 200:
                    st.info(f"Votre dossier est disponible: {url}")
                else:
                    st.warning(f"Votre dossier sera bientôt disponible: {url}")

    # FIXME: should proceed anyway ? Log the error ?
    error = open_and_check_issue(submission)
    if error:
        st.error(f"Could not open issue: {error}")

    # Write a json file with information about the submission
    write_json(submission)
    submission.validated = True
