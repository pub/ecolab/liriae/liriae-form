import streamlit as st
from streamlit_option_menu import option_menu

from liriae_form.utils import version


def page1():
    st.set_page_config(f"LIRIAe form (v{version})", initial_sidebar_state="collapsed")
    st.title("🌳 LIRIAe: liseuse et recherche intelligente pour les autorités environnementales")
    st.subheader(f"Formulaire de dépôt et modification de dossiers (v{version})")

    # 2. horizontal menu
    selected = option_menu(menu_title=None, options=["Présentation", "Aide et contact"], orientation="horizontal")

    if selected == "Présentation":
        st.info(
            """👏 Bienvenu sur l'interface LIRIAe ! Première visite ? Nous vous conseillons vivement d'accorder
                quelques minutes au visionnage du tuto de manipulation dans la page \" aide et contact \", elle
                vous sera d'une aide précieuse !"""
        )

        st.title("👋 Présentation")
        st.header("Qu'est ce que le projet LIRIAe ?")
        st.markdown(
            """Le projet LIRIAe a pour premier objectif de faciliter le travail des auditeurs de l'autorité
                environnementale dans leur travail d'évaluation de projets divers et variés sur le point très
                particulier de l'impact sur l'environnement."""
        )
        st.markdown(
            """Aujourd'hui, les dossiers soumis à évaluation sont pour la majorité mal construits ce qui rend leur
                évaluation longue et laborieuse d'autant plus que ces fichiers peuvent atteindre des tailles très
                conséquentes """
        )

        st.header("Pourquoi cet outil ?")
        st.markdown(
            """Le but du projet est d'intégrer le document pdf dans une liseuse intelligente qui, pour fonctionner,
                nécessite de prendre en entrer un pdf structuré. C'est l'objectif de cette interface qui sert de
                préparation à l'utilisation de la liseuse."""
        )

        st.header("Et alors, à quoi sert cette page ?")
        st.markdown(
            """La détection de titre dans un document quelconque étant une tâche informatique complexe,
            elle recquiert un petit coup de pouce de l'utilisateur pour que le traitement soit optimal
            et s'améliore au fur et à mesure. Vous avez bien lu ! Plus l'outil sera utilisé, plus
            l'équipe LIRIAe pourra mettre en place des outils performants (et plus votre travail de
            prétraitement sera court !)"""
        )
        st.markdown("Pour résumer, cette interface permet")
        st.markdown(" - de structurer votre document en le préparant pour la liseuse intelligente")
        st.markdown(" - d'adapter chaque document à votre utilisation personelle !")
        st.markdown(" - d'aider à la reconnaissance de titre en signalant des problèmes/imperfections")

    if selected == "Aide et contact":
        st.info(
            "💚 Sage décision, vous avez choisi de commencer par le commencement ! N'hésitez à \
             revenir sur cette page en cas de problème"
        )
        st.title("🆘 Aide et contact")

        # st.subheader("Vidéo explicative sur le fonctionnement de l'interface")

        st.subheader("Contacts")
        st.markdown("En cas de problème technique, vous pouvez contacter l'équipe LIRIAe par mail :")
