import functools
import json
import os
import sys
import tempfile
import time
from contextlib import contextmanager
from itertools import accumulate
from pathlib import Path

import boto3
import botocore
import fitz
import pkg_resources
import requests
import toml
from pdfstruct.collection import Collection
from smart_open import open

from liriae_form.schemas import InfoPdfstruct, PdfFile, Position, Submission, Toc, TocItem, doc_level

version = pkg_resources.get_distribution("liriae-form").version


@functools.lru_cache(maxsize=0 if "pytest" in sys.modules else 1)
def get_s3_client():
    try:
        return boto3.client(
            "s3",
            endpoint_url="https://" + os.environ["AWS_S3_ENDPOINT"],
            aws_access_key_id=os.environ["AWS_ACCESS_KEY_ID"],
            aws_secret_access_key=os.environ["AWS_SECRET_ACCESS_KEY"],
            aws_session_token=os.environ.get("AWS_SESSION_TOKEN", ""),
        )
    except (KeyError, ValueError):
        return None


@functools.lru_cache(maxsize=0 if "pytest" in sys.modules else 1)
def read_file(file: str, client=get_s3_client(), mode="rb", **kwargs):
    with open(file, mode=mode, transport_params={"client": client}, **kwargs) as f:
        return f.read()


@contextmanager
def open_pdf(pdf: str, client=get_s3_client(), mode="rb", **kwargs) -> fitz.Document:
    if not pdf.startswith("s3://"):
        yield fitz.open(pdf)
    else:
        with fitz.open(stream=read_file(pdf, client, mode, **kwargs)) as doc:
            yield doc


def split_s3_path(filename: str) -> tuple[str, str]:
    """Return the bucket and the key associated to the s3 path"""
    bucket, _, key = filename.replace("s3://", "").partition("/")
    return bucket, key


def get_file_size(filename: str) -> int:
    if filename.startswith("s3://"):
        bucket, key = split_s3_path(filename)
        return get_s3_client().head_object(Bucket=bucket, Key=key).get("ContentLength", 0)
    else:
        return os.stat(filename).st_size


def file_exists(filename: str) -> bool:
    try:
        get_file_size(filename)
    except FileNotFoundError:
        return False
    except botocore.exceptions.ClientError as e:
        return int(e.response["Error"]["Code"]) != 404
    return True


output_dir = os.environ.get("LIRIAE_OUTPUT_DIR", "outputs")
output_csv = os.path.join(output_dir, "dossiers.csv")
emails = (
    ", ".join(os.environ.get("LIRIAE_EMAIL_CONTACT", "").split(",")) if os.environ.get("LIRIAE_EMAIL_CONTACT") else None
)

result = requests.get("https://geo.api.gouv.fr/regions")
assert result.status_code == 200
regions = sorted(v["nom"] for v in result.json())


def write_file(uploaded_file, output_file_name):
    if not str(output_file_name).startswith("s3://"):  # N.B.: Path removes a "/" from "s3://"
        Path(output_file_name).parent.mkdir(parents=True, exist_ok=True)
    with open(output_file_name, mode="wb", transport_params={"client": get_s3_client()}) as f:
        f.write(uploaded_file.read())


def load_json(json_file: str):
    """Load the submission info from a json file
    Args:
        json_file (str): json file path
    """
    with open(json_file, transport_params={"client": get_s3_client()}) as f:
        submission = Submission.parse_obj(json.load(f))
        submission.new = False
        return submission


def write_json(submission: Submission):
    """Write a json file with information about the submission"""
    final_path_json = os.path.join(submission.output_dir, "FINAL_" + submission.request_id + ".json")
    with open(final_path_json, "w", transport_params={"client": get_s3_client()}) as outfile:
        outfile.write(submission.json(indent=2))


def run_pdfstruct(submission: Submission, pdf_path: str, pdf_selected: str):
    """functions that runs pdfstruct on pdf_path and update the dictionary pdf_state
       with pdfstruct information

    Args:
        submission (Submission): submission class
        pdf_path (str): pdf path
        pdf_selected (str): pdf name
    """

    if pdf_path.startswith("s3://"):
        # Create a temporary file to run pdfstruct
        with tempfile.NamedTemporaryFile() as tmp_pdf, open(
            pdf_path, mode="rb", transport_params={"client": get_s3_client()}
        ) as f:
            tmp_pdf.write(f.read())
            run_pdfstruct(submission, tmp_pdf.name, pdf_selected)
        # Fix pdf file path
        submission.pdf_files[pdf_selected].path = os.path.join(submission.output_dir, pdf_selected)
    else:
        start = time.time()
        collection = Collection.from_pdf(filename=pdf_path)
        first_pages = list(accumulate([1] + [len(d.pages) for d in collection.docs[:-1]]))
        pdf_file = submission.pdf_files[pdf_selected]
        toc = pdf_file.toc = Toc(parent=pdf_file)

        for n_doc, (doc, offset) in enumerate(zip(collection.docs, first_pages)):
            toc.insert_sorted(
                TocItem(
                    title=pdf_selected.strip(".pdf") if n_doc == 0 else f"Document_{n_doc + 1}",
                    page=doc.pages[0].id + offset,
                    level=doc_level,
                    position=Position(),
                    origin="pdfstruct",
                )
            )

            for s in doc.sections:
                toc.insert_sorted(
                    TocItem(
                        title=f"{s.kind.numbering} {s.kind.title}",
                        page=s.page + offset,
                        level=doc_level + s.kind.level,
                        position=Position(
                            x=float(s.x0),
                            y=float(s.y0),
                        ),
                        origin="pdfstruct",
                    )
                )

        time_running = time.time() - start
        try:
            d = toml.load("pyproject.toml")
            pdfstruct_version = d["tool"]["poetry"]["dependencies"]["pdfstruct"]["rev"]
        except (FileNotFoundError, KeyError):
            pdfstruct_version = pkg_resources.get_distribution("pdfstruct").version

        # Collect infos from pdfstruct running
        submission.info_pdfstruct[pdf_selected] = InfoPdfstruct(
            pdf_name=pdf_selected,
            pdfstruct_version=pdfstruct_version,
            time_running=time_running,
            n_docs=len(toc.docs),
            docs_first_pages=first_pages,
            collection=[doc.raw for doc in collection.docs],
        )


def concatenate_pdf(pdf_files: list[PdfFile]) -> fitz.Document:
    """
    Return a concatenated pdf from the list of PdfFile objects

    Args:
        pdf_files (list[PdfFiles]): PdfFile objects

    Returns: fitz.Document
    """
    toc = []
    pdf: fitz.Document = fitz.open()
    for pdf_file in pdf_files:
        # Loop over docs and take the toc of each one, in order to skip
        # the titles inside the docs that should not be kept
        for doc in pdf_file.toc.docs:
            if not doc.keep:
                continue
            for toc_item in pdf_file.toc.get_doc_toc(doc):
                if not toc_item.keep:
                    continue
                dest = {
                    "kind": 1,
                    "page": pdf.page_count + toc_item.page - 1,
                    "to": fitz.Point(toc_item.position.x, toc_item.position.y),
                    "zoom": 0,
                    "collapse": False,
                }
                toc.append([toc_item.level - doc_level + 1, toc_item.title, pdf.page_count + toc_item.page, dest])
            with open_pdf(pdf_file.path) as pdf2:
                pdf.insert_pdf(pdf2)
    pdf.set_toc(toc)
    return pdf


def open_issue(request_id: str, issue_iid: int | None = None, mention: str | None = None, **kwargs):
    """Open or add a note to an issue in gitlab with information about the submission

    Args:
        request_id (str): unique identifier for the issue
        issue_iid: int | None = None -> issue id to comment on, otherwise create a new one
        mention: str | None = None -> mention user(s) on issue (e.g.: @all)
        kwargs (dict): information to add to the description (e.g.: name="my name", mail="my@mail.com", ...)

    Returns:
        dict: gitlab API response
    """
    try:
        GITLAB_URL = os.environ["LIRIAE_DOSSIERS_GITLAB"]
        PROJECT_ID = os.environ["LIRIAE_DOSSIERS_PROJECT_ID"]
        ACCESS_TOKEN = os.environ["LIRIAE_DOSSIERS_TOKEN"]
    except KeyError:
        return

    # Set the issue title and description
    title = request_id
    mention = f"({mention})" if mention else ""
    description = f"Dossier reçu par LIRIAe form v{version} {mention}\n\n" + "\n\n".join(
        f"**{k}**: {v}" for k, v in kwargs.items()
    )

    # Make the POST request to the GitLab API
    url = f"{GITLAB_URL}/api/v4/projects/{PROJECT_ID}/issues"
    headers = {
        "Private-Token": ACCESS_TOKEN,
    }
    if not issue_iid:
        data = {
            "title": title,
            "description": description,
            "labels": "test" if os.environ.get("LIRIAE_DOSSIERS_TEST") else "reçu",
        }
    else:
        data = {"body": description}
        url += f"/{issue_iid}/notes"
    response = requests.post(url, headers=headers, data=data)
    return response


def open_and_check_issue(submission: Submission):
    """Open or add a note to the issue in gitlab and check if it worked fine. Otherwise, return error"""
    if "LIRIAE_DOSSIERS_GITLAB" in os.environ:

        response = open_issue(
            request_id=submission.request_id,
            issue_iid=submission.issue_iid,
            mention=os.environ.get("LIRIAE_DOSSIERS_MENTION"),
            email=submission.email,
            region=submission.region,
            name=submission.name,
            file_names=list(submission.pdf_files.keys()),
            flagged={
                name: pdf_file.flagged_description
                for name, pdf_file in submission.pdf_files.items()
                if pdf_file.flagged
            },
            etude_dimpact=submission.etude_dimpact,
            size_MB=submission.size_MB,
            output_dir=output_dir,
        )
        if response.status_code // 100 != 2:
            return response.json()
        else:
            submission.issue_iid = response.json()["iid"]


def set_values(doc: TocItem, doc_number: int, state: dict):
    """
    Callback function to change the toc item values. Retrieve values from st.session_state

    Args:
        doc: TocItem to be changed
        doc_number (int): document number in order
        state: st.session_state
    """

    doc.title = state[f"doc_title{doc_number}"]
    doc.page = state[f"doc_page{doc_number}"]
    doc.keep = state[f"keep_doc{doc_number}"] == "✅"


def change_toc_item(toc_item: TocItem, state: dict):
    title = state["toc_item_title"]
    level = state["toc_item_level"]
    toc_item.title = title
    toc_item.level = level
    if title == toc_item.original_title and level == toc_item.original_level:
        toc_item.modified = False
