import os

import streamlit as st

from liriae_form.st_utils import validate_submission
from liriae_form.utils import version


def page4():

    # Load state
    submission = st.session_state.submission

    st.set_page_config(f"LIRIAe form (v{version})", initial_sidebar_state="collapsed")
    st.title("🌳 LIRIAe: liseuse et recherche intelligente pour les autorités environnementales")
    st.subheader(f"Formulaire de dépôt et modification de dossiers (v{version})")

    st.header("Traitement de l'ensemble de vos fichiers")
    st.info(f"Vous traitez le dossier: {submission.request_id}")

    if submission.validated:
        st.success("BRAVO ! Votre document a été soumis avec succès !")
        st.info(
            f"Si vous voulez apporter des modifications, veuillez revenir à l'étape précédente \
            et resoumettre un traitement. Si vous voulez reprendre la modification à plus tard, \
            veuillez conserver le numéro d'identification : {submission.request_id}"
        )

    else:
        st.subheader("Résumé de votre document final")

        # Summary of all documents
        for pdf_name in submission.files:
            pdf_file = submission.pdf_files[pdf_name]
            docs_kept = pdf_file.toc.docs_kept
            if not docs_kept:
                st.markdown(pdf_name + " ❌", help="Aucun document sélectionné")
            elif pdf_file.flagged:
                st.markdown(
                    pdf_name + " 🚩",
                    help="Ce fichier sera traité manuellement par nos équipes."
                    " Pour l'instant, il sera ajouté en tant que tel."
                    " Nous reviendrons vers vous dans les plus brefs délais",
                )
            else:
                st.markdown(pdf_name)
            for item in docs_kept:
                st.write(f"-------------{item.title}")

        st.info(
            """Cette opération va générer un pdf qui sera la concatenation de tous les pdf que vous avez importé,
            auquel sera ajouté toutes les informations que vous avez renseigné précédemment. Ce dernier sera
            exploitable directement dans la liseuse de LIRIAe"""
        )

        if "LIRIAE_PDF_URL" in os.environ:
            if os.environ.get("LIRIAE_DOSSIERS_TEST"):
                st.warning("Les checkbox ci-dessous n'apparaissent pas dans la version prod")
                st.checkbox("Envoyer à l'API", value=True, key="do_send_to_api")
                st.checkbox(
                    "Indexer le document",
                    value=True,
                    key="do_index",
                    disabled=not st.session_state["do_send_to_api"],
                )
                st.checkbox(
                    "Utiliser liriae-back",
                    value="LIRIAE_BACK_URL" in os.environ,
                    key="use_liriae_back",
                    disabled=not st.session_state["do_index"] or "LIRIAE_BACK_URL" not in os.environ,
                )
                st.checkbox(
                    "Indexer les annotations",
                    value=True,
                    key="do_index_annotations",
                    disabled=not st.session_state["do_index"] or st.session_state["use_liriae_back"],
                )
            else:
                st.session_state["do_send_to_api"] = True
                st.session_state["do_index"] = True
                st.session_state["use_liriae_back"] = "LIRIAE_BACK_URL" in os.environ
                st.session_state["do_index_annotations"] = True

        col1, col2, col3 = st.columns([1, 1, 1])
        col2.button(
            "Démarrage traitement",
            on_click=validate_submission,
            kwargs=dict(submission=submission),
            # disabled=submission.validated,    # TODO: enable when it is set to False when something changes
        )
