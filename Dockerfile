FROM python:3.10

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1


COPY ./pyproject.toml ./pyproject.toml
COPY ./README.md README.md
COPY ./liriae_form liriae_form

RUN pip install --upgrade pip setuptools wheel \
    && pip install -e . \
    && pip cache purge \
    && rm -rf /root/.cache/pip

ENTRYPOINT ["streamlit", "run"]
CMD ["liriae_form/streamlit_app.py"]
