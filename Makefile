quality:
	isort . -c
	flake8
	mypy --install-types --non-interactive --implicit-reexport liriae_form/
	black --check .

# this target runs checks on all files and potentially modifies some of them
style:
	isort .
	black .

test:
	poetry run pytest --cov=liriae_form --cov-report=term --cov-report=html --cov-report=xml tests/ -vv

run:
	poetry run streamlit run liriae_form/app.py
