# liriae-form 

Projet de liseuse et recherche intelligente pour les autorités environnementales : formulaire de soumission de dossiers.

## Installation

```shell
git clone https://git.lab.sspcloud.fr/liriae/liriae-form.git
cd liriae-form
```

Installer [poetry](https://python-poetry.org/docs/) et le package:
```shell
curl -sSL https://install.python-poetry.org | python3 - # or pip install poetry
poetry install 
poetry shell
```

Ou:

```shell
pip install -e .
```

## Utilisation

```shell
streamlit run liriae_form/streamlit_app.py
```

### Où les informations et les fichiers sont enregistrés

Lors de la soumission d'un dossier, les informations sont enregistrées sur `$LIRIAE_OUTPUT_DIR/dossiers.csv` et les fichiers
sur `$LIRIAE_OUTPUT_DIR/<uuid>/`. Pour les versions déployées:

- prod: ```LIRIAE_OUTPUT_DIR=s3://projet-outil-ae/dossiers```
- dev: ```LIRIAE_OUTPUT_DIR=s3://projet-outil-ae/test/dossiers```

### Email de contact

Si la variable `$LIRIAE_EMAIL_CONTACT` est définie, le(s) mail(s) s'affiche(nt) à la page d'accueil et lors de l'envoi d'un dossier. 
Pour les versions déployées, la variable est définie via un secret Kubernetes (`liriae-email`).

### Taille maximale d'un fichier

La taille maximale d'un fichier est de 200 Mo par défaut et peut être définie à l'aide de la variable d'environnement (en Mo) `STREAMLIT_SERVER_MAX_UPLOAD_SIZE`.

### API liriae-pdf

Si les variables `$LIRIAE_PDF_URL`, `$LIRIAE_PDF_USER` et `$LIRIAE_PDF_USER_PWD` sont définies, le document final est envoyé à l'API et indéxé. Le lien vers LIRIAe est affiché, si `$LIRIAE_URL` est définie. 

## Tests

```shell
poetry install --with=test,quality
make style  # fix style
make quality # check quality
make test
```

